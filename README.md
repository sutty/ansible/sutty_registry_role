Sutty Registry Role
===================

Enables a private container registry.

Role Variables
--------------

| Variable   | Description       | Default value |
| ---------- | ----------------- | ------------- |
| `address`  | Registry address  | -             |
| `username` | Registry username | -             |
| `password` | Registry password | -             |

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "role"
    include_role:
      name: "sutty_registry_role"
    vars:
      address: ""
      username: ""
      password: ""
```

License
-------

MIT-Antifa
